---
layout: markdown_page
title: "Category Direction - Runner"
description: "GitLab Runner is the multi-platform execution agent that works with GitLab CI to execute the jobs in your pipelines. View more information here!"
canonical_path: "/direction/verify/runner/"
---

- TOC
{:toc}

## Vision

Our **vision** for GitLab Runner is to offer DevOps teams a build agent that works seamlessly on tomorrow's market-leading computing platforms and the tools to eliminate CI build fleet operational complexity at enterprise scale in support of [Ops Section Goals](/direction/ops/#gitlab-goals).

To better understand GitLab Runner's vision, let's discuss how this aligns with the GitLab product vision and strategy and considers the realities of the broader computing and technology landscape. At GitLab, our product [vision](https://about.gitlab.com/direction/#vision) is to replace DevOps toolchains with a single application that is pre-configured to work by default across the entire DevOps lifecycle. One of the identified strategic challenges along the way to realizing that vision is managing the balance between self-managed and GitLab.com product offerings. So, as is the case with the GitLab product strategy, the Runner three-year strategy must balance the requirements and projected evolution of both the SaaS and Self-Managed solutions.

So - why is that?  In ten years (2030), we can reasonably forecast that a significant percentage of organizations will use GitLab SaaS as their value stream delivery platform compared to organizations that choose the self-managed option. However, there will continue to be a subset of organizations that will require a self-managed solution. There will also be a percentage of customers who use a hybrid solution, i.e., GitLab SaaS with self-hosted runners.

Today, GitLab Runner is distributed as a binary or container image. Customers can deploy and host a fleet of runners on public cloud or on-premise infrastructure. Several machine architectures, including Intel (x86-64), ARM (ARM64), IBM z-series, and host operating systems, Linux, Windows Server, macOS, are supported. Industry watchers [forecast](/direction/ops/#market-predictions) that based on current trends, containers and microservices will dominate the technology landscape in the 2020s, with Kubernetes the platform of choice for container orchestration.  Of course, innovation and technology shifts can happen at any time, so we should anticipate that future technology solutions may change the market dynamics.

We can make at least one fundamental forecast that is likely to be accurate based on the computing industry's historical evolution. In 2030, much like today, there will continue to be several computing platforms and architectures used in the computing industry. And so, to enable the GitLab product vision, GitLab Runner must continue to evolve and seamlessly support the market-leading computing technologies, machine architectures, and container orchestration platforms. 

### Direction overview 

<iframe width="560" height="315" src="https://www.youtube.com/embed/yfk6x87No-Q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Organizational Adoption Journey

```mermaid
graph LR
   classDef purple fill:#696dbe, color:#fff
   subgraph cj1[Customer Journey]
   style cj1 fill:#121111,stroke:#121111,stroke-width:1px,color:#fff
      A0[User Actions]:::purple
   end
   subgraph ob1["POC, ONBOARDING, PRODUCTION ENVIRONMENT SETUP"]
   style ob1 fill:#121111,stroke:#121111,stroke-width:1px,color:#fff
      A1[Install GitLab]:::purple --> A2(Install the 1st Runner):::purple
   end
   subgraph sc1["SCALE USAGE PHASE: USERS, FEATURES, STAGE ADOPTION, n...n+1 phases"]
   style sc1 fill:#121111,stroke:#121111,stroke-width:1px,color:#fff
      A2 --> A3(Rollout and start managing more than 1 Runner to support the organization):::purple
   end
   A0-.-A1;
```

```mermaid
graph LR
   classDef orange fill:#f0a543, color:#00000
   subgraph cj1[Customer Journey]
   style cj1 fill:#ededf7,stroke:#ededf7,stroke-width:1px,color:#00000
      A0[Runner Pain Points]:::orange
   end
   subgraph ob1["POC, ONBOARDING, PRODUCTION ENVIRONMENT SETUP"]
   style ob1 fill:#ededf7,stroke:#ededf7,stroke-width:1px,color:#00000
      A1[Manual steps to install<br /> and configure first Runner]:::orange --> A2(Manual steps to configure additional Runners):::orange
   end
   subgraph sc1["SCALE USAGE PHASE: USERS, FEATURES, STAGE ADOPTION, n...n+1 phases"]
   style sc1 fill:#ededf7,stroke:#ededf7,stroke-width:1px,color:#00000
      A2 --> A3[Runner Token registration<br /> process is manual]:::orange
      A3 --> A4[High operational overhead<br /> to maintain a large fleet of Runners]:::orange
      A4 --> A5[No instrumentation that<br /> indicates why a Runner<br /> has not started]:::orange
   end
   A0-.-A1;
```

## Runner Product Development Categories

The Runner product development strategy comprises three main focus areas to enable the vision.
1. Runner Core: features and capabilities for the open-source GitLab Runner CI execution agent.
1. Runner Cloud: the GitLab Runner CI build virtual machines available on GitLab SaaS.
1. Runner Enterprise Management: features and capabilities for DevOps teams to simplify the management of the GitLab Runner CI/CD build infrastructure.

## Top Vision Item(s)

The content in this section outlines some of the initiatives that we are exploring. In some cases, we are in the early phases of exploring the concept, while in others, we have a more concrete solution in mind and have started work on the first iteration of features or capabilities.

### Runner Core

- [Autoscaling replacement for Docker Machine](https://gitlab.com/groups/gitlab-org/-/epics/2502) Docker Machine is the technology used to provide runners' autoscaling on virtual machines hosted on the major public cloud platforms. However, Docker Machine has been in [maintenance mode](https://github.com/docker/machine/issues/4537) for some time now. Even though we continue to maintain a [fork](https://docs.gitlab.com/runner/executors/docker_machine.html#forked-version-of-docker-machine) of Docker Machine, our long-term plan is to provide a GitLab supported replacement that enables the automated scale up and down of runners on public cloud-hosted virtual machines.
- [GitLab Runner: Kubernetes Executor-FY22](https://gitlab.com/groups/gitlab-org/-/epics/5451) The GitLab Runner Kubernetes executor enables you to run GitLab CI pipeline jobs on Kubernetes clusters, whether on-premise or delivered by public cloud providers. By taking advantage of Kubernetes autoscaling primitives, GitLab Runner plus the Kubernetes executor is an efficient and automated way to spin up pods to execute CI jobs at scale. In FY22, we plan to invest significantly in this capability by adding highly requested features and capabilities to this executor and providing first-class support for Kubernetes platforms such as Red Hat OpenShift.

For a more in-depth view of the issue backlog for Runner core, refer to the [issue list.](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARunner)

### Runner Cloud

- [macOS Build Cloud - Runners on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/1830) Our goal is to provide customers a GitLab CI solution that delivers exceptional build speed and on-demand scale building applications for Apple products, including iPad, iPhone, Apple Watch, Apple TV, and Mac.
- [Windows Runners on GitLab.com - GA](https://gitlab.com/groups/gitlab-org/-/epics/2162) In Q4 FY20, we released the preview of Windows Runners on GitLab.com. We have seen a gradual increase in the adoption and use of this service. Additionally, there has been quite a bit of feedback and requests for enhancements. Next up is maturing the [autoscaler driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler) for Windows, including adding pre-provisioning of VM's and transitioning this offer to GA.
- [Linux Runners on GitLab.com - Offer multiple GCP VM types and sizes](https://gitlab.com/groups/gitlab-org/-/epics/2426) We currently offer a single VM type, (Google Cloud Platform n1-standard-1 ), for Linux Runners on GitLab.com.  However, the execution duration of certain types of CI jobs can be reduced if they are executed on compute or memory optimized VM's, or general-purpose VM's with more vCPUs and RAM. Offering additional options will also help users who are not interested in managing their own Runners.

###  Runner Enterprise Management

- [Make CI easy for Self-Managed GitLab users:](https://gitlab.com/groups/gitlab-org/-/epics/2778) Our goal is that users self-managed GitLab should be able to get up and running with CI with as little friction as possible.
- [Runner Enterprise Management-New Runner Management UI:](https://gitlab.com/groups/gitlab-org/-/epics/4015) In the Runner Enterprise Management category, we are starting to collect customer feedback on delivering a first-class enterprise management experience in the GitLab UI for organizations that manage large Runners fleets to support the execution of thousands to millions of CI jobs monthly. This feature set is just one step on the journey to improve runners' management in a corporate environment and to deliver capabilities that our customers expect for an enterprise-class solution.

## What's Next & Why

In the section below, we highlight a few key features that we are working to deliver in the near term releases.

### Runner Core

- [Support git strategy with Kubernetes executor](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3847) As mentioned above, the Kubernetes executor is a critical component of the GitLab Runner strategy. Next up is adding support for the git_strategy CI/CD setting.
- [Red Hat Certified GitLab Runner for OpenShift GA](https://gitlab.com/groups/gitlab-org/-/epics/4453) In 13.7, we released a new GitLab Runner Operator](https://gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator) in Red Hat's Operator Hub beta channel. We plan to tranistion the runner Operator to the Red Hat Operator Hub stable channel.
- [Sticky Runners](https://gitlab.com/gitlab-org/gitlab/-/issues/17497) The MVC Sticky Runner feature will enable a user to specify that all jobs in a pipeline execute on a single GitLab Runner configured to use the [shell executor](https://docs.gitlab.com/runner/executors/shell.html). The goal is to implement a MVC solution that simplifies the passing of intermediate build data between stages in a pipeline.
- [Add support for Podman 2.2+ to GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27119) Docker is not included or supported on Red Hat Enterprise Linux 8. So our many users and customers on the Red Hat ecosystem need to be able to use [Podman](https://github.com/containers/podman), Red Hat's client tool for managing containers, as a GitLab Runner supported executor.

### Runner Cloud

- [macOS Build Cloud (Runners) on GitLab.com - Beta](https://gitlab.com/gitlab-org/gitlab/-/issues/269756) In Q1 FY22, we plan to release the GitLab macOS Build Cloud in beta, a build environment for the Apple ecosystem integrated with GitLab SaaS and powered by our open-source autoscaler. Feedback from the beta will inform our plans for autoscaler features, platform capacity, build queue, and OS image management.

## Who we are focusing on? 

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Runner, our "What's Next & Why" are targeting the following personas, as ranked by priority for support: 

1. [Devon - DevOps Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. [Sasha - Software Developer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer) 
1. [Delaney - Development Team Lead](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)

## Maturity Plan

This category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)).  However, we must defend our lovable status and provide the most value to our user community. To that end, we continue to collect and analyze customer and community feedback to understand users' pain points with installing, managing, and using GitLab Runners at scale. Besides working on the long-term vision, we continuously assess and prioritize working on issues opened by the community and customers in each release.

## Competitive Landscape

When you run a continuous integration pipeline job or workflow, it is powered by some computing platform - this commonly includes a container, a physical desktop computer, a bare-metal server, or a virtual machine environment. A  CI/CD agent is the software that is responsible for executing the scripts defined in a continuous integration pipeline job or workflow.  There is the GitLab Runner, Jenkins agent, Github runner, Azure Pipelines agents, Drone.io runners, Codefresh Runner, to name a few. 

In short, the GitLab Runner takes a job from a GitLab instance, executes that job, and sends the results back to GitLab, all over HTTPS.

So, given the seemingly simple CI job execution task, should we even consider the runner from a competitive analysis standpoint? The answer is yes, for as you can see from the few examples below, the industry recognizes that the CI  engine's capabilities are critical to a value stream delivery platform's long-term success in the market.

In [GitHub's 2021 Roadmap](https://github.com/github/roadmap/projects/1), we are seeing several runner related features including:
1. [Multiple Hosted Runner Sizes](https://github.com/github/roadmap/issues/95)
1. [Enterprise Server can use Actions hosted runners](https://github.com/github/roadmap/issues/72). 

Our perspective is that GitLab Runner is well-positioned to enable GitLab's strategic vision.  Why?

1. GitLab Runner is open-source. Our community members and customers have full access to the GitLab Runner source code and can contribute features, bug fixes directly to the code base.
1. GitLab Runner supports multiple executors. One of the most useful executor types is the Docker executor, which enables users to execute CI jobs inside a container, resulting in less maintenance for the CI/CD build environment.
1. GitLab Runner includes autoscaling out of the box. Autoscaling is available for public cloud virtual machines or Kubernetes stacks by using the Kubernetes executor.
1. GitLab Runner supports several computing architectures. Customers who need to self-host runners on platforms such as IBM Z mainframes to take advantage of GitLab and modern Value Stream Delivery Platforms can. We aim to meet and support customers on the platforms that they use in their environment.

### SaaS Build Agents

Users of SaaS hosted CI platforms, such as GitLab.com, CircleCI, and GitHub, Azure DevOps can run their CI pipelines and get to a first green build without being concerned about the agents' setup and maintenance. Like GitLab, these providers are managing the build agents and the infrastructure that those agents run on.

GitLab Runner is a lightweight, high performant build agent that executes CI jobs in the cloud very efficiently. The performance of CI jobs, measured in job duration minutes, on GitLab.com hosted runners relative to the market is very comparable. The one variable is the computing resources allocated to the virtual machines hosting the build agents. 

Today competitors such as GitHub, CircleCI, and Azure DevOps offer multiple virtual machine types for build agents hosted on Linux or Windows virtual machines. GitLab offers one machine type for both Linux and Windows virtual machines. However, users on GitLab.com can install their own GitLab runners on a supported computing platform and use those runners to execute the CI jobs in their GitLab.com hosted project repository. 

### Self-Managed Build Agents

For users who need to host and manage their own build agents and the underlying infrastructure, GitLab runner offers a wide array of solutions positioned very well in the market place from a competitive standpoint. First, GitLab Runner binaries are available on x86, AMD64, ARM64, ARM, s390x (IBM-Z mainframes). Linux, Windows, and macOS operating systems are fully supported. 

A significant competitive differentiator is the availability of the GitLab Runner [custom executor](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers). This open-source solution enables the community to add an executor to the GitLab Runner to execute CI jobs on a computing platform or infrastructure that's not currently supported. With this very powerful yet simple to implement driver, users can configure the GitLab Runner to use an executable to provision, run and clean up a build environment.

GitLab Runner is also available as a container image on [DockerHub](https://hub.docker.com/r/gitlab/gitlab-runner). Judging by the hundreds of thousands of daily downloads, this is a top-rated solution for users. 

Customers hosting build agents on public cloud virtual machines can quickly deploy and use the GitLab Runner autoscaling feature. This autoscaling solution enables the automated provisioning and decommissioning of build agents based on CI job load. 

GitLab Runner's support for Kubernetes and the recent addition of autoscaling capability for AWS ECS and Fargate makes the GitLab Runner a compelling option for enterprises considering which value stream delivery platform to deploy to support their digital transformation strategic goals.

## Top Customer Success/Sales Issue(s)

For the CS team, the issue [gitlab-runner#3121](https://gitlab.com/gitlab-org/gitlab-runner/issues/3121) where orphaned processes can cause issues with the runner in certain cases has been highlighted as generating support issues.

## Top Customer Issue(s)

- [gitlab-org#4976](https://gitlab.com/gitlab-org/gitlab/-/issues/14976): Runner Priority
- [gitlab-runner#2797](https://gitlab.com/gitlab-org/gitlab-runner/issues/2797): Local runner execution MVC
- [gitlab-runner#1057](https://gitlab.com/gitlab-org/gitlab-runner/issues/1057): Specify root folders for artifacts
- [gitlab-runner#6400](https://gitlab.com/gitlab-org/gitlab-runner/issues/6400): Make environment variables set in before_script available for expanding in .gitlab-ci.yml
- [gitlab-runner#1107](https://gitlab.com/gitlab-org/gitlab-runner/issues/1107): Docker Artifact caching
- [gitlab-runner#3392](https://gitlab.com/gitlab-org/gitlab-runner/issues/3392): Multi-line command output can be un-collapsed in Job terminal output view
- [gitlab-runner#3207](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3207): Configure docker volumes in .gitlab-ci.yml
- [gitlab-runner#1797](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1797): Caching is very slow for Node.JS projects

## Give Feedback

The near term features and capabilities highlighted here represent just a subset of the features and capabilities that have been requested by the community and customers. If you have questions about a specific runner feature request or have a requirement that's not yet in our backlog, you can provide feedback or open an issue in the GitLab Runner [repository](https://gitlab.com/gitlab-org/gitlab-runner/-/issues).
