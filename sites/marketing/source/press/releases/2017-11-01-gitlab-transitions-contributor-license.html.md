---
layout: markdown_page
title: "GitLab Transitions Contributor Licensing to Developer Certificate of Origin to Better Support Open Source Projects; Empower Contributors"
description: "GitLab today announced it was abandoning the industry-standard Contributor License Agreement in favor of a Developer Certificate of Origin (DCO) and license"
canonical_path: "/press/releases/2017-11-01-gitlab-transitions-contributor-license.html"
---

### Open source project customers Debian and GNOME announce plans to migrate communities to GitLab; applaud the direction

**SAN FRANCISCO - November 1, 2017** - GitLab, a software product used by [2/3 of all enterprises](/is-it-any-good/), today announced it was abandoning the industry-standard Contributor License Agreement (CLA) in favor of a Developer Certificate of Origin (DCO) and license. The DCO gives developers greater flexibility and portability for their contributions. The move has already attracted the attention of large open source projects who recognize the benefits. Debian and GNOME both plan to migrate their communities and open source projects to GitLab.

GitLab’s move away from a CLA is meant to modernize its code hosting and collaborative development infrastructure for all open source projects. Additionally, requiring a CLA became problematic for developers who didn’t want to enter into legal terms; they weren’t reviewing the CLA contract and they effectively gave up their rights to own and contribute to open source code.

“Many large open source projects want to be masters of their own destiny, but overly restrictive licensing can be a barrier to attracting talented contributors and driving innovation in the project,” said Sid Sijbrandij, CEO at GitLab. “With a DCO and license, developers no longer have to surrender their work and enter into legal terms. They will now have the freedom to contribute to open-source code and the flexibility to leverage their contributions as they need.”

In comparison, other platforms, like GitHub, Phabricator, Jenkins, and Elastic, all currently require a CLA. For established companies, a shift of this magnitude is not easy. But after evaluating the needs of larger open source projects such as Debian and GNOME, GitLab came to the conclusion that a DCO would better suit their efforts to modernize code hosting and collaborative-development infrastructure.

“We’re thrilled to see GitLab simplifying and encouraging community contributions by switching from a CLA to the DCO,” said Chris Lamb, Debian Project Leader. “We recognize that making a change of this nature is not easy and we applaud the time, patience and thoughtful consideration GitLab has shown here.”

 "We applaud GitLab for dropping their CLA in favor of a more OSS-friendly approach," said Carlos Soriano, Board Director at GNOME. "Open source communities are born from a sea of contributions that come together and transform into projects. This gesture affirmed GitLab's willingness to protect the individual, their creative process, and most importantly, keeps intellectual property in the hands of the creator."

In addition to moving toward DCO, GitLab will take internal actions to review code that is submitted, to help minimize the likelihood of anything problematic entering the base. GitLab has already begun making the switch with no added steps necessary from their user base. For more information on what this means for the broader GitLab community, please visit [this link](/blog/2017/11/01/gitlab-switches-to-dco-license/).

**About GitLab**
GitLab is a DevOps platform built from the ground up as a single application for all stages of the DevOps lifecycle enabling Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides a single data store, one user interface, and one permission model across the DevOps lifecycle. This allows teams to significantly reduce cycle time through more efficient collaboration and enhanced focus. Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprises, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network, and Comcast trust GitLab to deliver great software faster. GitLab is the world's largest all-remote company, with more than 1,200 team members in more than 65 countries and regions.


#### Media Contact
Natasha Woods
<br> 
GitLab
<br> 
press@gitlab.com
