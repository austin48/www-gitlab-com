---
layout: handbook-page-toc
title: "Data Team Organization"
description: "GitLab Data Team Organization"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

---

## <i class="far fa-compass fa-fw icon-color font-awesome" aria-hidden="true"></i> Team Organization

The Data Team is organized into centers of technical and subject matter excellence, completemented by __Business-Focused__ and __Business-Involved__ teams responsible for delivery data solutions that we call **Fusion Teams**.
Each Fusion Team supports one or more business divisions  and works with stable business counterparts to coordinate priorities and requirements.

The data fusion model emphasizes 2 elements:
1. Enagagement with Business Partners & Data Champions to build better products and drive towards the same goal
1. Visibility and Transparency around the plans, processes, and projects we use to reach those goals

| **Go To Market (GTM)** | **Product** | **G&A** |
| ----- | ----- | ----- |
| `@iweeks` _Lead/DRI_ | `@rparker2` (Acting) _Lead/DRI_	| `@rparker2` (Acting) _Lead/DRI_ | 
| `@jeanpeguero` |	`@mpeychet` |  |
| `@jjstark` | `@m_walker` |  |
| `@paul_armstrong` | `@snalamaru` | |
| `@ken_aguilar` |	`@laddula` | |	
						
### Engagement with Business Partners and Data Champions includes:

1. Bi-weekly Issue/Status updates
1. Monthly Priority Alignment
1. Quarterly OKR review
1. Every 6 months a CSAT survey to provide feedback to the Data Fusion Team towards the goal of continuous improvement

We encourage our stakeholders to follow along with our issue boards to understand the scope of work:

1. [GTM](https://gitlab.com/gitlab-data/analytics/-/boards/1912663?&label_name[]=ft%3Al2c)
2. [Product](https://gitlab.com/groups/gitlab-data/-/boards/1912130?label_name[]=ft%3Ar2a)
3. [G&A](https://gitlab.com/groups/gitlab-data/-/boards/1435002?&label_name[]=People) 

<!-- need to update boards potentially based on new label names -->

### Visibility

Beyond the fore-mentioned engagement, we also have Monthly Release Communication and demo sessions to help our stakeholders understand what we have developed as a Data Team and how it can be utilized.

**Monthly Release Communications**
<!-- (need to add issues with monthly release information or slide deck from OKRs -->
`Coming Soon`

**Demo Sessions**

Data Team Demos are available in [our YouTube Channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI). Every GitLab Team Member is invited to attend our [weekly Data Team Demo session](https://calendar.google.com/event?action=TEMPLATE&tmeid=Z2Zibm5rbWZvamptajYwOGs4dWI2ODk0c2tfMjAyMTAzMDRUMTYwMDAwWiBnaXRsYWIuY29tX2Q3ZGw0NTdmcnI4cDU4cG4zazYzZWJ1bzhvQGc&tmsrc=gitlab.com_d7dl457frr8p58pn3k63ebuo8o%40group.calendar.google.com&scp=ALL).

### Development Approach

To stay engaged and transparent in our work, we want our stakeholders to understand how the data team works and what they can expect out of us when we engage. We aim to follow the development process in the diagram below. This approach allows us to capture the requirements and goals of stakeholders upfront and include them in the process as we develop iteratively towards the end solution.
![data team development_process](data_team_development_process.png)

### Data Team Roles:

<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://about.gitlab.com/job-families/finance/data-analyst/" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Data Analyst</a>
  <a href="https://about.gitlab.com/job-families/finance/data-engineer/" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Data Engineer</a>
  <a href="https://about.gitlab.com/job-families/finance/manager-data" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Manager</a>
  <a href="https://about.gitlab.com/job-families/finance/dir-data-and-analytics" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Director of Data & Analytics</a>
</div>
