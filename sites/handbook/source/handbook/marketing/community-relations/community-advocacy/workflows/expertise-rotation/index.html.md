---
layout: handbook-page-toc
title: "Expertise workflows and training"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Community advocates have their own [expertises]/handbook/marketing/community-relations/community-advocacy/#expertises) (the expertises include EDU/OSS/Merch/Integrations), and we need to ensure that each expert creates and maintains training for their specialty, and makes sure other advocates are well versed in the processes. We ask each advocate to help in all expertises each week in the queue. Experts (designated advocates who know the most about a given expertise should allow for extra time each week to assist other advocates with Zendesk cases, questions, and escalations from their expertise.

## Training
Designated experts from each expertise are responsible for ensuring [training docs](/handbook/marketing/community-relations/community-advocacy/#tech-stack) in the handbook are kept up to date, as well as creating training sessions for advocates who need additional help learning an expertise. Advocates should do training sessions during the Community Advocacy daily planning calls when there are no any other important topics to discuss. 

## Regular daily workflow
The CA team consists of team members in the US, EU, and APAC. Advocates should block time in their workday to work each expertise.
