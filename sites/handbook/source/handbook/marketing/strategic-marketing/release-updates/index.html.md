---
layout: handbook-page-toc
title: "Product Release Updates"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Current updates

### 13.0–13.3 Sales enablement

The Q3 FY21 Product Release Update was August 20, 2020:

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Wldab4cx9OE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

The content was updated with more information on use of the GitLab Value Framework, including in [this presentation](https://docs.google.com/presentation/d/1GHePaoupd-4CMbwY2qyySba908ukTUHLgGyyV9lDZyY/edit?usp=sharing):

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/1GHePaoupd-4CMbwY2qyySba908ukTUHLgGyyV9lDZyY/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

Covering these releases:
*  2020-08-22 [GitLab 13.3 released with coverage-guided fuzz testing and a build matrix for CI/CD](/releases/2020/08/22/gitlab-13-3-released/)
*  2020-07-22 [GitLab 13.2 released with Planning Iterations and Load Performance Testing](/releases/2020/07/22/gitlab-13-2-released/)
*  2020-06-22 [GitLab 13.1 released with Alert Management and Code Quality Enhancements](/releases/2020/06/22/gitlab-13-1-released/)
*  2020-05-22 [GitLab 13.0 released with Gitaly Clusters, Epic Hierarchy on Roadmaps, and Auto Deploy to ECS](/releases/2020/05/22/gitlab-13-0-released/)

### 12.9–13.0 Sales enablement

The Q2 FY21 Product Release Update was June 4, 2020:

<figure class="video_container">
<iframe width="1027" height="642" src="https://www.youtube.com/embed/36-G7teGhwY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

With this presentation:

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/1NvRHQ8kgOdaaMJo8nnKw9ZzMJHbyDylNMFO1aErO7IA/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

Covering these releases:
*  2020-05-22 [GitLab 13.0 released with Gitaly Clusters, Epic Hierarchy on Roadmaps, and Auto Deploy to ECS](/releases/2020/05/22/gitlab-13-0-released/)
*  2020-04-22 [GitLab 12.10 released with Requirements Management and Autoscaling CI on AWS Fargate](/releases/2020/04/22/gitlab-12-10-released/)
*  2020-03-22 [GitLab 12.9 released with Vault App, Code Quality Reports and Group Deploy Tokens](/releases/2020/03/22/gitlab-12-9-released/)

### 13.0 Launch

GitLab 13.0 was launched on May 22, 2020 with [this release blog post](/releases/2020/05/22/gitlab-13-0-released/) and is our latest major release.

The launch featured this video of Scott Williamson, Chief Product Officer sharing 13.0 highlights and insight into key drivers of our product strategy looking ahead:

<figure class="video_container">
<iframe width="1198" height="674" src="https://www.youtube.com/embed/dStIhTP1ckg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Recent releases

For a list of all release posts including patch releases, see [this blog category, 'releases'](/releases/categories/releases/).

Find future releases, with their most important features flagged, on [Product's upcoming releases page](/upcoming-releases/). You can also see [upcoming features for paid product tiers, listed by tier](/direction/paid_tiers/).

Information about the release currently running on GitLab SaaS is at [the GitLab SaaS release page](/releases/gitlab-com/).

## Past updates

### Version 12 Year in Review

Leading into the GitLab 13.0 launch, [this blog post, including a video and slide deck](/blog/2020/05/21/version-12-year-in-review/) reviewed versions 12.0–12.10.

Major product themes are more apparent over this year of GitLab versions than they generally are in one month or quarter. For GitLab 12.x, these include "treating developers, security, and operations alike as first-class citizens in DevOps" and expanding the definition of DevOps and its practitioners, for example by building in Design Management and Requirements Management.

### 12.3–12.8 Sales enablement

This product release update covered 12.3–12.8 for FY21 Q1 and FY20 Q4, and was delivered 2020-02-27:

<figure class="video_container">
<iframe width="1174" height="642" src="https://www.youtube.com/embed/REF_-mEs4zc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

With this presentation:

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/18__GhONqgBzoi-rMmyUat1wzemz1mU3OdiJ3W2fb6kg/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

And in [these meeting notes](https://docs.google.com/document/d/1q35EN6PkbhM7uoHGDHdmWK7RyhLa3JPlPDvJdmaaxlw/edit?usp=sharing), covering these six releases:

*  2020-02-22 [GitLab 12.8 released with Log Explorer, NuGet, and Compliance](/releases/2020/02/22/gitlab-12-8-released/)
*  2020-01-22 [GitLab 12.7 released with Parent-Child Pipelines and Windows Shared Runners Beta](/releases/2020/01/22/gitlab-12-7-released/)
*  2019-12-22 [GitLab 12.6 released with Security Scorecard and Release Evidence](/blog/2019/12/22/gitlab-12-6-released/)
*  2019-11-22 [GitLab 12.5 released with EKS Cluster Creation & Environments Dashboard](/blog/2019/11/22/gitlab-12-5-released/)
*  2019-10-22 [GitLab 12.4 released with improved Merge Request Dependencies and Audit API](/blog/2019/10/22/gitlab-12-4-released/)
*  2019-09-22 [GitLab 12.3 released with Web Application Firewall and Productivity Analytics](/releases/2019/09/22/gitlab-12-3-released/)

### 12.0–12.2 Sales enablement

Our first quarterly, sales-focused product release update covered 12.0–12.2 for FY20 Q3 in August, 2019. [This recording on GitLab Unfiltered](https://www.youtube.com/watch?v=y2KrovJD76Q) shares highlights and analysis from [the shared notes document](https://docs.google.com/document/d/1T3Zsj90TkLD0wXXjsWQ6EhuEhtyzjXKd1EOhbxZGI3g/).

In addition to product highlights, the session called out new business value and selling opportunities in Ultimate or Premium, along with what to expect in GitLab 12.3–12.5.

Releases covered:
*  2019-08-22 [GitLab 12.2 released with Directed Acyclic Graphs for Pipelines and Design Management](/releases/2019/08/22/gitlab-12-2-released/)
*  2019-07-22 [GitLab 12.1 released with Parallel Merge Trains and Merge Requests for Confidential Issues](/releases/2019/07/22/gitlab-12-1-released/)
*  2019-06-22 [GitLab 12.0 released with Visual Reviews and Dependency List](/releases/2019/06/22/gitlab-12-0-released/)

### 12.0 Launch

The GitLab 12.0 launch included a livestreamed program: [11.0 to 12.0: How we got to DevSecOps in a single application](https://www.youtube.com/watch?v=ZopqBQQOK20), and in that, an Ask Me Anything featuring GitLab CEO Sid Sijbrandij.

> GitLab 12.0 marks a key step in our journey to create an inclusive approach to DevSecOps, empowering 'everyone to contribute'.
>  
>  We believe everyone can contribute, and we’ve enabled cross-team collaboration, faster delivery of great code, and bringing together Dev, Ops, and Security.

Read more in the 12.0 release blog post: [GitLab 12.0 released with Visual Reviews and Dependency List](/releases/2019/06/22/gitlab-12-0-released/).

### Version 11 Year in Review

GitLab's Version 11 year in review provided an overview of what we released, 11.0 to 12.0 as we moved into DevSecOps:

<figure class="video_container">
<iframe width="1141" height="642" src="https://www.youtube.com/embed/sboJfUylJFA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### 11.0 Launch

The theme of our 11.0 launch was [Auto DevOps](/stages-devops-lifecycle/auto-devops/) (as it had been for 10.0):

> Beyond making it easy to host and collaborate on public and private repositories, GitLab also simplifies the rest of the process by offering the whole delivery toolchain, built in. And now, it's not only built in, it's automated. Simply commit your code and Auto DevOps can do the rest. Auto DevOps is a pre-built, fully featured CI/CD pipeline that automates the entire delivery process.  

Read more in the 11.0 release blog post: [GitLab 11.0 released with Auto DevOps and License Management](/releases/2018/06/22/gitlab-11-0-released/).

### 10.0 Launch

The theme of our 10.0 launch was [Auto DevOps](/stages-devops-lifecycle/auto-devops/):

> From the formulation of an idea to executing and monitoring it in production, DevOps establishes a culture and environment where developing, testing, and releasing software can happen quickly, frequently, and more reliably.
>
> GitLab 10.0 delivers a hands-free DevOps environment with the introduction of Auto DevOps, allowing your team to easily configure and adopt modern development practices in your workflow.

Read more in the 10.0 release blog post: [GitLab 10.0 released with Auto DevOps and Group Issue Boards](/releases/2017/09/22/gitlab-10-0-released/).

## Planned updates

Product Marketing delivers quarterly product release update Sales enablement sessions. These are aligned with the product year, beginning with annual, major releases like 13.0 on May 22, 2020.

Near a major release, Product Marketing also produces a "year in review" and a forward-looking launch of the new version. Monthly release blog posts continue to announce monthly releases with Product Marketing support.

Version 13 enablements include a 13.0–13.2 quarterly for May–July, 13.3–13.5 for August–October, 13.6–13.8 for November–January, and 13.9–13.11 for February–April, leading into a Version 13 Year in Review and 14.0 Launch in May, 2021.

*GitLab's annual, major release moved up by one month before Version 13, leaving Version 12's last release as 12.10 rather than 12.11, to track our product year quarters with those of our fiscal year. We turned the dial on release updates by one notch to match, so as of July, 2020 the Version 13 quarterly enablements also track with the product and fiscal year.*

## Purpose

Product Marketing delivers quarterly and annual, sales-centric updates of:

*  How recent product releases enhance GitLab's value proposition for solving customers' business problems
*  How these releases enhance the value of Premium and Ultimate
*  What is coming in the next quarter

Product Marketing Managers also serve as [Messaging Leads for monthly product release blog posts](/handbook/marketing/blog/release-posts/#messaging-lead), writing the headline and introduction, identifying key features, and ensuring strong messaging.

This page merges release resources from Product and Marketing with related Sales Enablement pages, videos, and presentations. A "[handbook first](/handbook/handbook-usage/#why-handbook-first)" approach makes resources more findable and usable by Sales or anyone interested, and provides a stronger foundation for learning.
