---
layout: handbook-page-toc
title: Growth Sub-department
description: "The Growth Sub-department consists of development teams building growth experiments and managing privacy focused product intelligence"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## FY2021 Direction

Develop a respectful and privacy focused data collection framework that allows us to make informed product decisions and improve the customers experience, and promote conversion, adoption of key stages, and increase our user base.

## Mission

The Growth sub-department consists of groups that eliminate barriers between our users and our product value.

**Deliver product intelligence data to improve the GitLab product**

[Product Intelligence Group](/handbook/engineering/development/growth/product-intelligence/): Product Intelligence focuses on providing GitLab's team with data-driven product insights to build a better GitLab. To do this, we build data collection and analytics tools within the GitLab product in a privacy-focused manner. Insights generated from Product Intelligence enables us to identify the best places to invest people and resources, what product categories mature faster, where our user experience can be improved, and how product changes impact the business. You can learn more about what we're building next on the [Product Intelligence Direction page](/direction/product-intelligence/).

**Drive value for the business and our users by improving trial to paid conversion, per-stage adoption, and wider usage of GitLab**

We focus on validating ideas with data across the following four Groups:

[Activation Group](/handbook/engineering/development/growth/activation/)
- Adopt Verify [KPI](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-verify-stage-adoption-rate)

[Adoption Group](/handbook/engineering/development/growth/adoption/)
- Adopt Create [KPI](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-create-stage-adoption-rate)

[Conversion Group](/handbook/engineering/development/growth/conversion/)
- Trial conversion rate [KPI](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-trial-to-paid-conversion-rate)

[Expansion Group](/handbook/engineering/development/growth/expansion/)
- Invite team members [KPI](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-with-at-least-two-users-added)

The Growth groups also provide development input into two Growth product areas of focus:

1. [First Mile](https://gitlab.com/groups/gitlab-org/-/epics/4745) (New user flow)
2. [Continuous onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4817)

We work closely with the [Data Team](/handbook/business-ops/data-team/) along with our [Product Team](/handbook/product/categories/#growth-stage)
counterparts to design and implement experiments that measure the impact of changes to our messaging, UX, and overall experience of using GitLab.

## Groups

The Growth sub-department uses the `~"devops::growth"` label and the following groups for tracking throughput and ownership of issues and merge requests.

| Group name                                 | Group label                   |
| ----------                                 | -----------                   |
| [Product Intelligence](product-intelligence/)    | `~"group::product intelligence"` |
| [Activation](activation/)                  | `~"group::activation"`        |
| [Adoption](adoption/)                      | `~"group::adoption"`          |
| [Conversion](conversion/)                  | `~"group::conversion"`        |
| [Expansion](expansion/)                    | `~"group::expansion"`         |


## People leaders in development


<%=
departments = ['Activation', 'Conversion', 'Expansion' , 'Adoption', 'Product Intelligence']
department_regexp = /(#{Regexp.union(departments)})/

direct_team(role_regexp: department_regexp, manager_role: 'Director of Engineering, Threat Management and Growth')
%>

## All Team Members

The following people are permanent members of groups that belong to the Growth sub-department:

### Activation
<%= department_team(base_department: "Activation Team") %>

### Adoption
<%= department_team(base_department: "Adoption Team") %>

### Conversion
<%= department_team(base_department: "Conversion Team") %>

### Expansion
<%= department_team(base_department: "Expansion Team") %>

### Product intelligence

#### Product Intelligence Backend
<%= department_team(base_department: "Product Intelligence Team") %>

#### Product Intelligence Frontend
<%= department_team(base_department: "Product Intelligence FE Team") %>

## Business Continuity - Coverage and Escalation

The following table shows who will provide cover if one or more of the Growth Engineering management team are unable to work for any reason.

| Team Member     | Coverered by           | Escalation     |
| -----           | -----                  | -----          |
| Wayne Haber     | Christopher Lefelhocz  | Eric Johnson   |
| Jerome Ng       | Phil Calder            | Wayne Haber    |
| Phil Calder     | Jerome Ng              | Wayne Haber    | 

If an Engineer is unavailable the Engineering Manager will reassign open issues and merge requests to another engineer, preferably in the same [group](#all-team-members).

Some people management functions may require escalation or delegation, such as BambooHR and Expensify.

This can be used as the basis for a business continuity plan (BCP),
as well as a general guide to Growth Engineering continuity in the event of one or more team members being unavailable for any reason.

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%=
role_regexp = /[,&] (Growth|Product Intelligence)/
direct_manager_role = 'Director of Engineering, Growth'
other_manager_roles = [
  'Fullstack Engineering Manager, Product Intelligence',
  'Fullstack Engineering Manager, Growth:Activation, Adoption, Conversion and Expansion'
]
stable_counterparts(role_regexp: role_regexp, direct_manager_role: direct_manager_role, other_manager_roles: other_manager_roles)
%>

## How We Work
As part of the wider Growth stage we track and work on issues with the label `~"devops::growth"`.

### Product Development Flow

Our team follows the [Product Development Flow](/handbook/product-development-flow/#workflow-summary) utilizing all labels from `~workflow::start` to `~workflow::verification`.

We adhere to the **Completion Criteria** and **Who Transitions Out** outlined in the [Product Development Flow](/handbook/product-development-flow/#workflow-summary) to progress issues from one stage to the next.

#### Workflow Boards

We use workflow boards to track issue progress throughout a milestone. Workflow boards should be viewed at the highest group level for visibility into all nested projects in a group.

There are three GitLab groups we use:
- The [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) group includes the [gitlab](https://gitlab.com/gitlab-org/gitlab), [customers-gitlab-com](https://gitlab.com/gitlab-org/customers-gitlab-com), and [license-gitlab-com](https://gitlab.com/gitlab-org/license-gitlab-com) projects.
- The [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com/) group includes the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) project.
- The [gitlab.com/gitlab-services](https://gitlab.com/gitlab-services/) group includes the [version-gitlab-com](https://gitlab.com/gitlab-services/version-gitlab-com) project.

| gitlab-org | gitlab-com | gitlab-services | all groups |
| ------ | ------ | ------ | ------ |
| [Growth Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [Growth Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) |
| [Activation Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [Activation Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) |
| [Conversion Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [Conversion Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) |
| [Expansion Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [Expansion Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) |
| [Adoption Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [Adoption Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) |
| [Product Intelligence Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [Product Intelligence Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) |

### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. This is normally done in the monthly planning meeting.

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements.
| 13| A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues.

In planning and estimation, we value [velocity over predictability](/handbook/engineering/#velocity-over-predictability). The main goal of our planning and estimation is to focus on the [MVC](/handbook/values/#minimal-viable-change-mvc), uncover blind spots, and help us achieve a baseline level of predictability without over optimizing. We aim for 70% predictability instead of 90%. We believe that optimizing for velocity (MR throughput) enables our Growth teams to achieve a [weekly experimentation cadence](/handbook/product/growth/#weekly-growth-meeting).

- If an issue has many unknowns where it's unclear if it's a 1 or a 5, we will be cautious and estimate high (5).
- If an issue has many unknowns, we can break it into two issues. The first issue is for research, also referred to as a [Spike](https://en.wikipedia.org/wiki/Spike_(software_development)), where we de-risk the unknowns and explore potential solutions. The second issue is for the implementation.
- If an initial estimate is incorrect and needs to be adjusted, we revise the estimate immediately and inform the Product Manager. The Product Manager and team will decide if a milestone commitment needs to be adjusted.

### Product Development Timeline

Our work is planned and delivered on a monthly cycle using [milestones](https://docs.gitlab.com/ee/user/project/milestones/). Our team follows the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) utilizing all dates including from `M-1, 4th: Draft of the issues` to `M+1, 4th: Public Retrospective`.

#### Milestone Boards

We use milestone boards for high level planning and roadmapping across several milestones.

| gitlab-org | gitlab-com | gitlab-services |
| ------ | ------ | ------ |
| [Growth Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [Growth Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) |
| [Activation Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [Activation Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) |
| [Conversion Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [Conversion Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) |
| [Expansion Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [Expansion Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) |
| [Adoption Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [Adoption Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) |
| [Product Intelligence Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [Product Intelligence Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) |

### UX
The Growth UX team has a [handbook page](/handbook/engineering/ux/stage-group-ux-strategy/growth/) which includes [Growth specific workflows](/handbook/engineering/ux/stage-group-ux-strategy/growth/#how-we-work).

### How We Use Issues

To help our team be [efficient](/handbook/values/#efficiency), we explicitly define how our team uses issues.

#### Issue Creation

We aim to create issues in the same project as where the future merge request will live. For example, if an experiment is being run in the [GitLab CustomersDot](https://customers.gitlab.com), both the issue and MR should be created in the [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com) project.

We emphasize creating the issue in the right project to avoid having to close and move issues later in the development process. If the location of the future merge request cannot be determined, we will create the issue in our catch-all [growth team-tasks project](https://gitlab.com/gitlab-org/growth/team-tasks/issues).

We use [issue templates](#experiment-issue-templates) for common tasks.

#### Iteration

To support [Iteration](/handbook/values/#iteration) Growth engineering:

1. Separates refactoring from feature [MVCs](/handbook/values/#minimal-viable-change-mvc). When refactoring is raised in review, the preference is to resolve in a follow up issue.
1. Addresses technical debt and follow up issues through prioritization and discussion by relevant stakeholders.
1. Issues with a weight of `5` and higher should be reassigned to the Product Manager to make sure they can be split into smaller [MVCs](/handbook/values/#minimal-viable-change-mvc).
   When this is not possible, the Product Manager will create a spike or research issue so that engineering can break it down and close the original.
1. We make use of research issues and engineering spikes in the prior milestone, so estimations are more accurate.

#### Work affecting Salesforce.com

In an effort to prevent accidental outages to business critical processes, whenever we touch code which may have a direct or indirect impact on data flowing into Salesforce.com, we should assign @jbrennan1 to the MR as a reviewer and add the `~"Affects Salesforce"` label.

### Team performance indicators

* [Growth Performance Indicators]
* [Growth OKRs]

## Running Experiments

We follow a four step process for running experiments as outlined by [Andrew Chen's How to build a growth team.](https://andrewchen.co/how-to-build-a-growth-team/)

1. **Form Hypotheses:** Define ideas our team wants to test.
2. **Prioritize Ideas:** Decide which ideas to test first.
3. **Implement Experiments:** Do the Product, Design, Engineering, Data, and Marketing work to execute the experiment.
4. **Analyze Results:** Dive into the results data and prove or disprove our hypotheses.

Each week, we provide progress updates and talk about our learnings in our [Growth Weekly Meeting](/handbook/product/growth/#weekly-growth-meeting).

The duration of each experiment will vary depending on how long it takes for experiment results to reach statistical significance. Due to the varying duration, there will be some weeks when we have several experiments running concurrently in parallel.

### Experimentation Process

#### Overview

1. Create a [Growth experiment] issue to define the scope of the experiment (labels: `~"growth experiment"`, `~"experiment idea"`)
  - Anybody who has an idea of something to try as an experiment can create this issue
  - They provide information about which portion of the product they'd like to experiment with, their experimentation idea (i.e. their hypothesis), and what a successful outcome might look like
  - At this extremely early stage, the most critical thing is that we get the idea jotted down with enough context and detail that it is understood by others on the team and can be carried forward by the Project Manager
1. Project Managers (PM) review the [Experiment backlog](https://gitlab.com/groups/gitlab-org/-/boards/2028884) regularly to ensure they meet the necessary criteria of a well-defined experiment
   - experiments that meet our [Experiment Definition Standards](#experiment-definition-standards) and are deemed relevant are initially prioritized by adding the appropriate milestone list (`%Awaiting further demand`, `%Backlog`, `%Next 1-3 releases`, or a specific milestone)
   - experiments that do not meet our [Experiment Definition Standards](#experiment-definition-standards) or are deemed no longer relevant are closed
1. PM follows the process outlined in [Experiment Setup](#experiment-setup) to create an epic and related issues
1. PM, UX, and Engineering follow the [product development flow](https://about.gitlab.com/handbook/product-development-flow/) and create `workflow::` issues linked to the epic to track the work required to complete the experiment
1. Additionally, the PM works with the Data Team to define what data will be needed in order to fulfill the defined success metric
   - **Note:** In order for the Data Team to begin ingesting data from new tables or columns, those tables or columns must first exist in the production database.
1. Engineering teams deliver changes in line with our product development flow, being mindful of changes affecting other GitLab teams.
  - Our processes, planning, and current priorities are transparent to other groups
  - We try to avoid running experiments on features that are currently or soon to be under development by other groups
  - We choose reviewers and domain experts from relevant groups where possible to ensure different groups are familiar with the changes we are introducing
  - When our product managers make the call, we initiate experiment and feature flag cleanup, either adding the experiment feature to the product or reverting
1. Engineering & PM collaborate on getting the experiment activated according to the defined rollout plan
   - PM or Engineering update the [experiment tracking](#experiment-tracking-issue) issue, setting the experiment scoped label to `~"experiment::active"` when the experiment is live
   - PM or Engineering notify interested parties (their team, data team, support, TAMs, etc.) by at-mentioning them in a comment on the [experiment tracking](#experiment-tracking-issue) issue, and on the `#production` Slack channel
1. PM monitors the experiment via data sent to Sisense, adjusting the rollout strategy in the [experiment tracking](#experiment-tracking-issue) issue as necessary
1. PM compares recorded data to the experiment's criteria for measuring success
   - if experiment (variant) is successful, PM/Engineering create an [Experiment Cleanup Issue](#experiment-cleanup-issue) to fully integrate the successful flow into the product
   - if experiment (all variants) are unsuccessful, PM/Engineering create an [Experiment Cleanup Issue](#experiment-cleanup-issue) to remove the experimentation code, reverting back to the "control" flow
1. Once the [experiment cleanup](#experiment-cleanup-issue) issue is resolved, the [experiment tracking](#experiment-tracking-issue) issue and [Experiment Epic](#experiment-epic) are closed and the experimentation process is complete

See also the [Growth RADCIE and DRIs](https://about.gitlab.com/handbook/product/growth/#growth-radcie-and-dris) for determining DRIs at each stage.

#### Experiment Issue Boards

A backlog of experiments are tracked on the [Experiment backlog](https://gitlab.com/groups/gitlab-org/-/boards/2028884?&label_name[]=growth%20experiment) board.

To track the status of an Experiment, Experiment tracking issues using the `~"experiment tracking"` and scoped `experiment::` labels are tracked on Experiments tracking boards across the following groups:

| gitlab-org | gitlab-com | all groups |
| ------ | ------ | ------ | ------ |
| [Experiment tracking](https://gitlab.com/groups/gitlab-org/-/boards/1352542) | [Experiment tracking](https://gitlab.com/groups/gitlab-com/-/boards/1542208) | [Issues List](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=growth%20experiment&label_name[]=experiment%20tracking) |

#### Experiment Setup

- Once the experiment hypothesis & measurement criteria are well-defined, an [Experiment Epic](#experiment-epic) is created at the top-level group most appropriate for the experimentation work (usually the `gitlab-org` group)
  - The epic becomes the single source of truth (SSoT) for the experiment
  - The original [Experiment Definition Issue](#experiment-definition-issue) is closed and gets attached to the epic
  - The final definition, hypothesis, & measurement criteria from the [Experiment Definition Issue](#experiment-definition-issue) are copied into the new epic's description
- New issues related to the experiment (such as an [experiment tracking](#experiment-tracking-issue) issue, an [experiment cleanup](#experiment-cleanup-issue) issue, a UX spec issue, or an Engineering work issue) are attached to the epic

##### Experiment Definition Issue

This issue acts as the starting point for defining an experiment, including an overview of the experiment, the hypothesis, and some idea of how success will be measured. This issue will be tagged with the `~"growth experiment"` and `~"experiment idea"` labels. The [Growth Experiment] issue template can be used for this.

##### Experiment Definition Standards

1. The experiment can be qualitatively measured (i.e. there are no other factors that could give us false data)
1. The experiment is as atomic as possible (i.e. it is easiest to measure one, specific, individual change rather than a series or group of changes)
1. The experiment has clearly defined and reliably measurable success metrics
   - There is consensus around the chosen success metric
   - The success metric is clearly tied to a piece of data which will be collected & measured during the active phase of the experiment ([experiment tracking](#experiment-tracking-issue) issue)
1. The experiment includes an estimated time to significance: how long the experiment would need to run given the population size and expected conversion rate
   - Can use handy tools, like the [A/B-Test Calculator (external)](https://abtestguide.com/calc/), to help calculate this estimate

##### Experiment Epic

This epic acts as the single source of truth (SSoT) for the experiment once an experiment has been properly defined according to our [Experiment Definition Standards](#experiment-definition-standards) and is deemed worthwhile to run. Once an [Experiment Definition Issue](#experiment-definition-issue) is added to this epic, we fill out further details such as the expected rollout plan. We also assign the experiment to a milestone and follow the product development flow for UX & Engineering work. As the experiment design and rollout progresses, this epic or parent issue should contain details or links to further information about experiment including the tracking events and data points used to determine if the experiment is a success as well as links to relevant metrics-reporting dashboards (such as Sisense).

##### Experiment Tracking Issue

This issue is used to track the experiment progress once deployed. It is similar to a Feature Flag Roll Out issue with an additional `experiment::` scoped label to track the status of the experiment. The [Experiment Tracking] issue template includes an overview of the experiment, who to contact, rollout steps, and a link to the overall experiment issue or epic.

The `experiment::` scoped labels are:

* `~"experiment::pending"` - The experiment is waiting to be deployed
* `~"experiment::active"` - The experiment is active (live)
* `~"experiment::blocked"` - The experiment is currently blocked
* `~"experiment::validated"` - The experiment has been validated (the success criteria was clearly met)
* `~"experiment::invalidated"` - The experiment has been invalidated (the success criteria was clearly unmet)
* `~"experiment::inconclusive"` - The experiment was inconclusive (the success criteria was not clearly met nor clearly unmet)

#### Experiment Cleanup Issue

This issue is used to clean up an experiment after an experiment has been completed. It is created within the project where the cleanup work will be done (e.g. the `gitlab-org/gitlab` project). The cleanup work may include completely removing the experiment (in the case of `~"experiment::invalidated"` and `~"experiment::inconclusive"`) or refactoring the experiment feature for the long run (in the case of `~"experiment::validated"`). The cleanup issue should be linked to the experiment tracking issue as a reference to ensure the experiment is concluded prior to cleanup.

The [Experiment Successful Cleanup] issue template can be used for the `gitlab-org/gitlab` project.

#### Experiment issue templates

- Growth team-tasks
  - [Growth team planning] issue template
  - [Growth Experiment] issue template for Growth teams
  - [Growth team-tasks experiment Tracking] issue template for Growth teams - use the GitLab project template where appropriate
- GitLab project
  - [Experiment Tracking]
  - [Experiment Successful Cleanup] issue template for `gitlab-org/gitlab`
  - [Experimentation] issue template for Engineering

## Experiment cadence

The Growth sub-department tracks number of experiments per engineer as a development metric.

<% if ENV['PERISCOPE_EMBED_API_KEY'] %>
  <div>
    <embed width="100%" height="575" src="<%= signed_periscope_url(chart: 10621867, dashboard: 799224, embed: 'v2') %>">
  </div>
<% else %>
  You must set a <code>PERISCOPE_EMBED_API_KEY</code> environment variable to render this chart.
<% end %>

## Growth Engineering Weekly

Every week, engineers in the Growth sub-department meet to discuss topics related to growth engineering. Discussion topics include how to track experiments, A/B testing, changes in CustomersDot, changes in gitlab application, etc. Growth Engineers are encouraged to bring discussion topics to the meeting and to them to the [agenda](https://docs.google.com/document/d/1VMj16-tvJg4m26y6q7A1jSdBD895ImFM2fbXvFXF4yM/edit?usp=sharing).

To get the most time zone coverage, these meetings alternate fortnightly between:
* Wednesdays 3:00PM UTC for US/EMEA
* Wednesdays 8:00PM UTC for US/APAC

Team members are encouraged to attend the meeting that matches their time zone.

## Team Days

On occasion we put together a virtual team day to help take a break and participate in fun, social activities across Engineering, Product, UX, Data and Quality.

* [May 2020](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/119)
* [September 2020](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/175)

## Common Links

* [How to create a Sisense SQL alert](/handbook/engineering/development/growth/sisense_alert.html)
* [Growth sub-department]
* [Growth workflow board]
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth) (GitLab internal)
* [Product Intelligence issues board]
* `#g_product_intelligence` in [Slack](https://gitlab.slack.com/archives/g_product_intelligence) (GitLab internal)
* [Growth technical debt status](https://app.periscopedata.com/app/gitlab/618368/Growth-technical-debt-status) (GitLab internal)
* [Growth opportunities]
* [Growth meetings and agendas] (GitLab internal)
* [GitLab values]


[Growth Experiment]: https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/new?issuable_template=Growth%20experiment
[Growth team-tasks experiment Tracking]: https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/new?issuable_template=experiment_tracking_template\
[Growth team planning]: https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/new?issuable_template=growth_team_planning_template

[Experiment Tracking]: https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=experiment_tracking_template
[Experiment Successful Cleanup]: https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Experiment%20Successful%20Cleanup
[Experimentation]: https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Experimentation

[GitLab values]: /handbook/values/
[Growth sub-department]: /handbook/engineering/development/growth/
[Growth workflow board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847
[Growth opportunities]: https://gitlab.com/gitlab-org/growth/product/-/issues
[Growth meetings and agendas]: https://drive.google.com/drive/search?q=type:document%20title:%22Growth%20Weekly%22
[Growth Performance Indicators]: /handbook/engineering/development/performance-indicators/growth/
[Growth OKRs]: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Growth%20Sub-Department&label_name[]=OKR
[Product Intelligence issues board]: https://gitlab.com/gitlab-org/product-intelligence/-/issues
