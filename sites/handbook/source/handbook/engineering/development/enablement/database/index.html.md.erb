---
layout: handbook-page-toc
title: Database Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

Developing solutions for scalability, application performance, data growth and developer enablement especially where it concerns interactions with the database.

## Mission

Focusing on the database, our mission is to provide solutions that allow us to scale to our customer's demands.  To provide tooling to proactively identify performance bottlenecks to inform developers early in the development lifecycle.  To increase the number of database maintainers and provide database best practices to the community contributors and development teams within GitLab.

## Team Members

The following people are permanent members of the Database Team:

<%= direct_team(manager_slug: 'craiggomes', role_regexp: /Database/) %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%#= stable_counterparts(role_regexp: /[,&] Database/, direct_manager_role: 'Backend Engineering Manager, Memory & Database') %>
<table>
<thead>
<tr>
<th>Person</th>
<th>Role</th>
</tr>
</thead>
<tbody>
<tr>
<td>Fabian Zimmer</td>
<td><a href="/job-families/product/product-manager">Principal Product Manager, Geo</a></td>
</tr>
</tbody>
</table>

## Meetings

Whenever possible, we prefer to communicate asynchronously using issues, merge requests, and Slack. However, face-to-face meetings are useful to establish personal connection and to address items that would be more efficiently discussed synchronously such as blockers.

* Database Group Office Hours every Tuesday and Thursday at 1:00 PM UTC
  * Tuesdays - first 30 minutes dedicated to team weekly topics
  * Thursdays - the first Thursday after milestone close is dedicated to the milestone retro
* [Database Office Hours](https://docs.google.com/document/d/1wgfmVL30F8SdMg-9yY6Y8djPSxWNvKmhR5XmsvYX1EI/edit#heading=h.oyp8amyknnr8) (internal link); [YouTube recordings](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp-kqXeiF7fF7cFYaKtdqXM)
  * Wednesdays, 3:30pm UTC (bi-weekly)
  * (APAC) Thursdays, 3:30am UTC (bi-weekly, alternating)

## Work

We follow the GitLab [engineering workflow](/handbook/engineering/workflow/) guidelines.  To bring an issue to our attention please create an issue in the relevant project.  Add the `~"group::database"` label along with any other relevant labels.  If it is an urgent issue, please reach out to the Product Manager or Engineering Manager listed in the [Stable Counterparts](/handbook/engineering/development/enablement/database/#stable-counterparts) section above.

### What we do

The team is responsible for the PostgreSQL application interactions to enable high performance queries while offering features to support scalability and strengthen availability.  PostgreSQL is the heart of Rails application, and there is no shortage of work to make GitLab more performant, scalable, and highly available from database perspective.  Some of the current priorities include implementing partitioning to improve query performance and creating tooling to enable development teams to implement their own partitioning strategies more easily.  We are working on tools that will help developers "shift left" in their migration testing prior to deployment.  We are always looking for ways to continuously care for the performance of our databsae and improve our developer documentation.  For more in-depth details of what we are working on please review our [Roadmap](#roadmap) section below.

### Planning
We use a [planning issue](https://gitlab.com/gitlab-org/database-team/team-tasks/-/blob/master/.gitlab/issue_templates/Planning.md) to discuss priorities and commitments for the milestone.  This happens largely asynchronously, but when we do need to discuss synchronously we discuss during the Tuesday team [meeting](#meetings) timeslot.

### Boards

[Database by Milestone](https://gitlab.com/groups/gitlab-org/-/boards/1318796?&label_name%5B%5D=group%3A%3Adatabase)
The Milestone board gives us a "big picture" view of issues planned in each milestone.

[Database: Validation](https://gitlab.com/groups/gitlab-org/-/boards/2305758?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Adatabase&label_name[]=database%3A%3Avalidation)
The validation board is a queue for incoming issues for the Product Manager to review. A common scenario for the Database Team validation board is when an issue is created that requires further definition before it can be prioritized. The issue typically states a big picture idea but is not yet detailed enough to take action. The Database Team will then go through a refinement process to break down the issue into actionable steps, create exit criteria and prioritize against ongoing efforts. If an issue becomes too large, it will be promoted to an epic and small sub-issues will be created.

[Database: Triage](https://gitlab.com/groups/gitlab-org/-/boards/2305765?scope=all&utf8=%E2%9C%93&label_name[]=database%3A%3Atriage)
The triage board is for incoming issues that require further investigation for team assignment, prioritization, previously existing issues, etc. Within the Database Group we have implemented a weekly triage rotation where one team member is responsible for monitoring this board for timely responses.

#### Say/Do Ratio
We use the `~Deliverable` label to track our Say/Do ratio.  At the beginning of each milestone, during a Database Group Weekly meeting, we review the issues and determine those issues we are confident we can deliver within the milestone.  The issue will be marked with the `~Deliverable` label.  At the end of the milestone the successfully completed issues with the `~Deliverable` label are tracked in two places.  We have a dashboard in Sisense that will calculate how many were delivered within the milestone and account for issues that were moved.  Additionally, our milestone retro issue lists all of the `~Deliverable` issues shipped along with those that missed the milestone.

#### Roadmap
The Database Group [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Roadmap&label_name[]=group%3A%3Adatabase) gives a view of what is currently in flight as well as projects that have been prioritized for the next 3+ months.


### Documentation

We document our insights, road maps and other relevant material in this section.

1. [Database Lexicon - terms and definitions relating to our Database](doc/lexicon.html)
1. [Database Strategy: Guidance for proposed database changes](doc/strategy.html)
1. [On table partitioning](doc/partitioning.html) (February 2020)
1. [Postgres: Sharding with foreign data wrappers and partitioning](doc/fdw-sharding.html)
1. [Sharding GitLab by root namespace](doc/root-namespace-sharding.html)
1. [Sharding with CitusDB](doc/citus.html) (April 2020)
1. [Table partitioning: Issue group search as an example](doc/issue-group-search-partitioning.html) (March 2020)
1. [Working with the GitLab.com database for developers](doc/gitlab-com-database.html)
1. [Database schema proposals for Container Registry](doc/container-registry.html) (September 2020)
1. [Workload analysis for GitLab.com](doc/workload-analysis.html) (October 2020)


### Performance Indicators (Internal)

1. [Enablement::Database - Performance Indicators Dashboard](https://app.periscopedata.com/app/gitlab/754160/Enablement::Database---Performance-Indicators)
1. Average Query Apdex for GitLab.com
   - [Target: 100ms - Tolerable 250ms](https://tinyurl.com/yxe4pv4a)
   - [Target: 50ms - Tolerable 100ms](https://tinyurl.com/y6latcuc)

### Common Links

* Slack Channel [#g_database](https://gitlab.slack.com/app_redirect?channel=g_database)
* [Database Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Adatabase)
* [Database Subgroup](https://gitlab.com/gitlab-org/database-team) - Issues and templates related to team processes.
* [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline)
* [YouTube: Database Team Playlist](https://www.youtube.com/watch?v=BqwsRDpknfg&list=PL05JrBw4t0KoxfN-uO2YfvQUabp2kdUYT)
* [YouTube: Database Office Hours Playlist](https://www.youtube.com/watch?v=p3ful2h8H-c&list=PL05JrBw4t0Kp-kqXeiF7fF7cFYaKtdqXM)
