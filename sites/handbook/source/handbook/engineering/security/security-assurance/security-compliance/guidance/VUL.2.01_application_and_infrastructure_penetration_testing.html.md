---
layout: handbook-page-toc
title: "VUL.2.01 - Application & Infrastructure Penetration Testing Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# VUL.2.01 - Application & Infrastructure Penetration Testing

## Control Statement

Penetration testing is performed for both the application and infrastructure annually. Results are evaluated and remediated according to risk rating.

## Context

This control is meant to formalize the way GitLab prioritizes our penetration tests. The rating assignment mentioned in this control is detailed in a separate control linked below. It isn't feasible to test 100% of GitLab systems and since penetration tests are meant to reduce risk to the organization, it makes sense that risk is the method we use for prioritizing which systems we test in a given year.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

Control Owner:

* Senior Director of Security

Process Owner:

* Application Security Team - Hacker1 and External 3rd-Party vendor relationship
* Red Team, Security - provide supplemental/enhanced penetration testing
* Infrastructure - Responsible for infrastructure penetration testing

## Guidance

We will need to share our methodology for determining which systems to pen test and that methodology should align with the related control.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Application & Infrastructure Penetration Testing control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/939).

### Policy Reference

## Framework Mapping

* ISO
  * A.12.6.1
* SOC2 CC
  * CC7.1
* PCI
  * 11.3
  * 11.3.1
  * 11.3.2
  * 11.3.4
