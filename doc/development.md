# Local development

## Gitpod.io

Gitpod.io is a "Dev Environments Built for the Cloud" third-party service, which can be used as an alternative to setting up a dev environment on your local machine.

See [doc/gitpod.md](./gitpod.md) in this repo for more details. 

## Install prerequisites

1. If you are on macOS, install [Homebrew](https://brew.sh/), which is a
   package manager for macOS that allows you to easily install programs
   and tools through the Terminal. Visit their website for installation
   instructions.
1. Install [nvm](https://github.com/nvm-sh/nvm), a Node version
   manager. Visit their website for installation instructions.
   After you have installed it, run the following in the directory for
   `www-gitlab-com`:

   ```sh
   nvm install
   nvm use
   ```

1. Install [yarn](https://classic.yarnpkg.com/en/docs/install), a package manager for
   the node ecosystem.

1. Install [rbenv](https://github.com/rbenv/rbenv), a Ruby version
   manager (You can use an alternative such as [RVM](https://rvm.io/),
   or [chruby](https://github.com/postmodern/chruby)). Visit their
   website for installation instructions.
1. **DO NOT** use the system Ruby. Use the ruby version manager to
   install the current [`www-gitlab-com` Ruby
   version](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.ruby-version).
   With `rbenv` for example, run the following in the directory for
   `www-gitlab-com`:

   ```sh
   # rbenv will install the ruby specified in .ruby-version
   rbenv install

   # Configure the shell environment for rbenv
   eval "$(rbenv init -)"
   # or append rbenv init to your ~/.bash_profile or ~/.zshrc
   echo 'eval "$(rbenv init -)"' >> ~/.bash_profile

   # set your ruby version to the target version of the project
   rbenv local <ruby version specified in .ruby-version>
   ```
1. Install bundler by running the following in the directory for `www-gitlab-com`:

   ```sh
   gem install bundler
   ```
1. **DO NOT** use the system Python. Use [Homebrew](https://brew.sh/) to install python or a python
   version manager such as [pyenv](https://github.com/pyenv/pyenv/wiki#suggested-build-environment).

   ```sh
   # Install python 3 with brew
   brew install python

   # or install pyenv with brew and use pyenv to manage python versions
   brew install pyenv
   pyenv install 3.8.2
   # Initialize pyenv; add this to your ~/.bash_profile
   eval "$(pyenv init -)"
   # Set 3.8.2 as the default version
   pyenv global 3.8.2
   ```

## Run middleman

Use the following commands to run Middleman in development mode (see [monorepo docs](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/doc/monorepo.md) for more details):

```sh
bundle install
yarn

cd sites/XXX # NOTE: where XXX is one of the sites in the monorepo. For example: `cd sites/handbook` or `cd sites/marketing`

NO_CONTRACTS=true bundle exec middleman

Refer to this issue for more context around the NO_CONTRACTS env variable: https://github.com/middleman/middleman/issues/2094
```

If you run into any issues with local development, running the command with an additional verbose flag may be helpful.
```sh
NO_CONTRACTS=true bundle exec middleman --verbose
```

Once the Middleman server is running, you can visit
[http://localhost:4567](http://localhost:4567) in your browser to see a live,
local preview of the site. Any changes to files in the `source` directory will
be detected automatically, and your browser will even reload the page if necessary.

PDF files are not available in development mode. See below for more information.

See the [Middleman docs](https://middlemanapp.com/basics/development_cycle/) for
more information.

### Faster local template development 

The Middleman Preview server can be slow to work with when you want to make changes to CSS, JavaScript, or Markup. We've resolved the CSS and JavaScript challenges by [using Webpack to serve assets without Middleman rebuilds](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/71845), but working on templating still triggers Middleman sitemap rebuilds. 

If you're a developer, designer, or other team member making changes to templates, we have an experimental option to speed up your development cycle: [a monkeypatch on the Middleman::Sitemap::Extensions::OnDisk::update_files method](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/73487). 

To enable it, run the Middleman preview server with the environment variable `DISABLE_ON_DISK_REBUILD=true`. 

This will allow you to make changes to templates and get much faster refreshes. It disables the `ensure_resource_list_updated!` method from running when template files are changed. The preview server still renders the template by calling its `render` method, so your markup changes should go through, but it may not work in every case (i.e. if you make a change that would require a new sitemap object to be created). 

Note that this change won't necessarily speed up data-driven pages. Making changes to YAML files in the `data` directory will still trigger the sitemap rebuild. The current implementation is rough and untested. Use at your own risk and please file issues and MRs if you would like to improve the overall experience. 

We have currenlty only added this to the `sites/marketing/config.rb` file since the [Digital Experience](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/#principles-wip) team put it together - but we have notified the handbook team of the option if they want to add it to `sites/handbook`. 

Using this in conjunction with [NO_CONTRACTS](https://github.com/middleman/middleman/issues/2094) may make your local development experience much faster. You can also make these commands slightly more ergonomic by [setting up aliases](https://linuxize.com/post/how-to-create-bash-aliases/) in your shell config file. An example for zsh might look like this: 

```sh
# ~/.zshrc

# Run the standard Middleman preview server from your repository
alias serve_marketing="cd /path/to/www-gitlab-com/sites/marketing; bundle exec middleman serve"

# Run the preview server without contract - faster serving, full Middleman functionality intact.
alias serve_marketing_faster="cd /path/to/www-gitlab-com/sites/marketing; NO_CONTRACTS=true bundle exec middleman serve" 

# Run the preview server without contracts and without rebuilding the sitemap - faster serving, faster template development, some Middleman features may not work. 
alias serve_marketing_fastest="cd /path/to/www-gitlab-com/sites/marketing; DISABLE_ON_DISK_REBUILD=true NO_CONTRACTS=true bundle exec middleman serve"

# Run the preview server without contracts, without rebuilding the sitemap, and with livereload enabled - livereload may slow things down substantially, but could be more convenient. 
alias serve_marketing_fastest_livereload="cd /path/to/www-gitlab-com/sites/marketing; DISABLE_ON_DISK_REBUILD=true ENABLE_LIVERELOAD=1 NO_CONTRACTS=true bundle exec middleman serve"
```

### Check Blog Categories

You can execute rake tasks locally too, just as CI/CD would run them. The following example checks if all
blog categories match. 

```
NO_CONTRACTS=true bundle exec rake lint:blog:categories

=> Checking if any posts have incorrect categories...
All posts have correct categories!
```


## Enable livereloading

When running middleman with the livereload option enabled, it watches your
repo for changes and reloads the site automatically.

Livereload can result to [slow server response times][gh-livereload], so it is
disabled by default. That means you need to manually refresh the webpage if you
make any changes to the source files. To enable it, just set the environment
variable `ENABLE_LIVERELOAD=1` before running middleman:

```
ENABLE_LIVERELOAD=1 bundle exec middleman
```

You can verify that it's enabled from the following line:

```
== LiveReload accepting connections from ws://192.168.0.12:35729
```

To permanently have livereload enabled without typing the environment variable,
just export its value in your shell's configuration file:

```
# Open your rc file (replace editor with vim, emacs, nano, atom, etc.)
editor ~/.bashrc

# Export the livereload variable
export ENABLE_LIVERELOAD=1
```

>**Note:**
You need to logout and login in order for the changes to take effect. To
temporarily use the changes, run `source ~/.bashrc`.

[gh-livereload]: https://github.com/middleman/middleman-livereload/issues/60

## Preview `/direction/`

The [direction](https://about.gitlab.com/direction/) page is generated
by a mix of [markdown text](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/template.html.md.erb)
and content generated automatically by fetching data from different projects. In order to fetch that data, the build process will need
a GitLab access token.

For the sake of build speed while developing the website, the direction page is
only built when the `INCLUDE_GENERATORS` (and `PRIVATE_TOKEN` is set). This means that locally,
it returns a 404.

A workaround for previewing it locally is setting `INCLUDE_GENERATORS` and `PRIVATE_TOKEN`
and doing a local build (not development mode):

1. Open a terminal window on `www` and build the website:

    ```shell
    INCLUDE_GENERATORS=true PRIVATE_TOKEN=your_access_token bundle exec rake build:all
    ```

    Your [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
    can be generated in your GitLab.com's profile **Settings > Access Tokens**.

1. Wait until it builds (it takes about 10 min or more)
1. The website will be built in a folder called `/public/` in the root dir
1. `cd` to the `/public/` folder in another terminal
1. Run a local web server: `ruby -rwebrick -e'WEBrick::HTTPServer.new(:Port => 8000, :DocumentRoot => Dir.pwd).start'`
1. Preview at port 8000: `http://localhost:8000/direction/`

Also, for the sake of speed, the issue info is cached for 24h in a local file cache
at `./tmp/cache/direction/`. If you want to requery the GitLab servers for updated
issue information, you can delete that cache locally (if you're using a local
development environment), or run a GitLab.com pipeline with the key
`CLEAR_DIRECTION_CACHE` set to clear it on the remote runner.

Notes:

- You'll have to build the site again to preview a new change

## Troubleshooting

1. Do check that the Ruby version matches the [expected Ruby
   version](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.ruby-version).
   Run the following in the directory for `www-gitlab-com`:

   ```sh
   ruby --version
   ```

1. If a page is not loading with a `File not found` error, check if it is marked as ignored in the middleman sitemap. This is typically located at `http://localhost:7654/__middleman/sitemap/`. If it is marked as ignored, it may be prevented from rendering because you have not set your `PRIVATE_TOKEN` environment variable. This environment variable should be set to a GitLab [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with read-only permissions. Certain pages require this token to be built properly.

## Modern JavaScript

Currently a lot of the JavaScript developed locally, can be found in
[source/javascripts/](../source/javascripts/). The files there however are
handcrafted. For a more modern approach of developing interactive javascript
features, please have a look at [source/frontend/](../source/frontend/).

## Style

### Anchor/links and image tags

When adding anchor and image tags prefer the `link_to` and `image_tag` Middleman
helpers respectively instead of using `%a` and `%img` tags directly. This allows
Middleman to generate the URLs for those tags, which gives us greater control
and flexibility on URL configuration.

For example:

Instead of:

```haml
%img.event-tile-image{ src: "/images/20-years-open-source/1983-gnu-project.svg", alt: "20 years open source gnu project gitlab svg" }
```

Use:

```haml
= image_tag "/images/20-years-open-source/1983-gnu-project.svg", class: "event-tile-image", alt: "20 years open source gnu project gitlab svg"
```

Instead of:

```haml
%a.feature-more{ href: "/blog/2018/03/05/gitlab-for-agile-software-development/" } Read
```

Use:

```haml
= link_to "Read", "/blog/2018/03/05/gitlab-for-agile-software-development/", class: "feature-more"
```

For more information on middleman helpers go to https://middlemanapp.com/basics/helper-methods/

## Testing

### Rspec Unit and Integration Tests

To run all rspec tests:

```sh
bundle exec rspec
```

To run just rspec unit tests:

```sh
bundle exec rspec --tag ~@feature
```

To run just rspec integration tests:

```sh
bundle exec rspec --tag ~@feature
```

#### Faster Integration Tests

To make iterate faster on rspec integration tests in local dev by not having Capybara run Middleman (saves ~70 seconds):

* Start your local Middleman dev server with `RAILS_ENV=test` (this prevents flaky tests, animations are disabled in the test environment):

```
NO_CONTRACTS=true RAILS_ENV=test bundle exec middleman
```

* Then set the `CAPYBARA_LOCALHOST=true` environment var when you run your tests:

```sh
CAPYBARA_LOCALHOST=true bundle exec rspec path/to/integration_spec.rb
```

#### Debugging Integration Tests

If an integration test fails, you should get output like this in yellow to view the page and screenshot:

```
  HTML screenshot: /Users/username/workspace/www-gitlab-com/tmp/capybara/screenshot_2020-03-03-13-03-29.141.html
  Image screenshot: /Users/username/workspace/www-gitlab-com/tmp/capybara/screenshot_2020-03-03-13-03-29.141.png
```

To see the live browser while the Capybara integration tests run, set `CHROME_HEADLESS=false` (or `no` or `0`):

```sh
CHROME_HEADLESS=false bundle exec rspec path/to/integration_spec.rb
```

### Jest Javascript Tests

Run all js/jest tests:

```sh
yarn run test
```
