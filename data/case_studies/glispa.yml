title: Glispa
cover_image: '/images/blogimages/glispa.jpg'
cover_title: |
  Glispa successfully migrated to Kubernetes in 3 months with GitLab
cover_description: |
  Glispa needed to move to Kubernetes and used GitLab to seamlessly migrate to a DevOps workflow.
canonical_path: "/customers/glispa/"
twitter_image: '/images/blogimages/glispa.jpg'

twitter_text: 'Making the necessary move to Kubernetes, Glispa chose GitLab for a swift and easy transition.'

customer_logo: '/images/case_study_logos/glispa.png'
customer_logo_css_class: brand-logo-tall
customer_industry: Mobile advertising
customer_location: Berlin, Germany
customer_solution: GitLab Premium
customer_employees: 251 employees
customer_overview: |
   Making the necessary move to Kubernetes, Glispa chose GitLab for a swift and easy transition.
customer_challenge: |
  Trouble with orchestration issues led Glispa to seek out GitLab in order to migrate to Kubernetes.

key_benefits:
  - |
    Easily integrates with Kubernetes, Docker, Jira
  - |
    Unlimited user access points
  - |
    Automated reporting
  - |
    Flexible CI pipeline templates

customer_stats:
  - stat: 30%
    label: Cost savings over AWS
  - stat: 846
    label: Projects
  - stat: 20
    label: Runners with 1000 jobs per runner

customer_study_content:
  - title: the customer
    subtitle: High tech advertising firm
    content:
      - |
        Glispa is a mobile advertising technology company based in Berlin, Germany. With over a decade in the advertising industry, Glispa provides customers with efficient solutions to reach targeted audiences. The company approaches customer needs with four core pillars in mind: technology, trust, transparency, and team.

  - title: the challenge
    subtitle: Cowboy installation spurs pipeline hiccups
    content:
      - |
        [Glispa](https://www.glispa.com) was using DCOS for orchestration, but the engineering team was struggling to manage the pieced together platform. The team was looking for a way to reduce headaches, improve pipelines, and be free from constantly doing fixes. Their goal was to embrace  DevOps practices and less Agile strategies in order to streamline processes.
      - |
        Management wanted to move away from the stressors of their pieced-together installation and start using Kubernetes. “I was head of the line, I managed the migration. My first six months here all I did was all the migration from DCOS to Kubernetes,” said Paride Martino, senior system engineer. Martino has previous experience with Kubernetes and knew that the only way to properly migrate over is to use a tool with proper version control that will integrate easily. GitLab is the only tool that is fully optimized for [Kubernetes migration](/solutions/kubernetes/).
      - |
        GitLab was adopted organically at Glispa and teams had initially been using the tool without any directives. Martino had previous experience using GitLab. So, he and the engineering team decided on GitLab Premium to meet their checklist of required capabilities. “You cannot have the proper installation at all with other products or other services. So if you want something in house in your own data center, it's GitLab,” Martino said. “Yeah, we started with GitLab, we are with GitLab.”

  - blockquote: With GitLab and Kubernetes, I can have three deploys a day — unlike the classic release cycle of every three months … I know from my steps that the test has passed, and I can promote automatically from staging to production directly in GitLab.
    attribution: Paride Martino
    attribution_title: Senior System Engineer at Glispa

  - title: the solution
    subtitle: Single source of truth, several points of access
    content:
      - |
        The Kubernetes installation was completed in well under six months. The engineering team is now focused on streaming pipelines and speeding up deployments. With GitLab, Glispa finally has all of its pipelines in one place. “You don't have the CI in other products as it is in GitLab. Almost all of our projects have some pipeline attached to it. Having everything in GitLab, we can manage permissions in one product,” Martino said. “We don’t have to manage the permissions in GitHub, and then permissions somewhere else, and then the permission in the Docker registry. It's only one permission set. That's why GitLab makes things easier for us.” With GitLab, Glispa teams have end-to-end insight and visibility.
      - |
        Engineers are also pleased with the ease of user accessibility. “It's much easier to manage users in your installation. That's a big plus,” Martino said. “And, we don't have to create users. We just click the 'sync' and we have the users there. That's much easier to manage for the help desk guys.”
      - |
        There is no set of tricky permissions for users to gain access and even guests can have the same rights as other users. “With other tools, it's either write permission or read-only. But with GitLab, if we choose to have a consultant here for a week, we just give guest access, for example. Things you can not do on GitHub,” Martino said. “You have three or four degrees of access you can give. Or even put in the token with a time limit … After three weeks it will automatically close. We couldn't do this with the other tools.”

  - blockquote: GitLab is so good, you won't even notice it.
    attribution: Paride Martino
    attribution_title: Senior System Engineer at Glispa

  - title: the results
    subtitle: Interminable DevOps workflow
    content:
      - |
        What Glispa has come to appreciate most about GitLab is that the tool works so seamlessly that teams hardly notice it at all. “I always say, I'm a system engineer. It means that you should not notice me working. I need to stay in the shadows. If you see me working, something is bad,” Martino said.
      - |
        The same is true of infrastructure -- it should just work without anyone noticing that it is functioning. “Your product is pure infrastructure at the end of the day,” Martino said. “If you notice GitLab, something is wrong. If you don't notice your program, then you're doing right. Because it just works and you don't even notice it's there.”
      - |
        GitLab is the single application for their entire DevOps lifecycle. CI templates empower how GitLab works uninterrupted with other tools. “There is flexibility because I'm using the templates of the [GitLab CI pipeline](/stages-devops-lifecycle/continuous-integration/), this flexibility allows me to do the complex work and the developers don't even notice. That's a big plus. I don't have to debug the pipelines. I need only to make sure that my template works.” From building Java projects to using Docker images, Martino uses what GitLab provides and creates from there. “We use GitLab to store code, build our solutions, build a Docker container, and deploy to Kubernetes,” Martino said.

